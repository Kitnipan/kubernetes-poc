VERSION=$(git describe --exact-match --tags $(git log -n1 --pretty='%h'))

echo $VERSION
docker build -t kitnipandh/hello-world:$VERSION .
docker push kitnipandh/hello-world:$VERSION

kubectl --kubeconfig="../cluster.yaml" get nodes

cd .. && cd kubernetes-deployment
git pull
rm deployment.yaml
cp deployment_template.yaml deployment.yaml
sed -i "s/###/$VERSION/g" deployment.yaml
kubectl --kubeconfig="../cluster.yaml" apply -f deployment.yaml --validate=false
kubectl --kubeconfig="../cluster.yaml" apply -f load-balancer.yaml --validate=false
git add .
git commit -n -m "⬆️ Update version to $VERSION"
git tag $VERSION
git push --tag
git push origin master